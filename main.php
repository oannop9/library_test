<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>main</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
	<div class="container-xl container-login100">
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="row">
					<div class="col-4"></div>
					<div class="col-4 p-4">
						<h1 class="text-center text-light" style="font-size: 4rem">Library</h1>
					</div>
					<div class="col-3 ">
						<button class="btn btn-success pr-3"
							onclick="window.location.href='borrow_form.php'">Borrow</button>
						<button class="btn btn-dark pr-3"
							onclick="window.location.href='return_book.php'">Return</button>
					</div>
					<div class="card col-sm-4" style="width: 13rem;">
						<img src="images/member.png" class="card-img-top" alt="...">
						<div class="card-body">
							<h5 class="card-title text-center">MEMBER</h5>
							<p class="card-text text-center">
								สัตว์อื่นปรุงแต่งใจให้เป็นบ้าไม่ได้แต่มนุษย์ปรุงแต่งจิตใจจนกระทั่งกลายเป็นบ้าไปแล้วก็มี
							</p>
							<center><a href="member.php" class="btn btn-primary mt-4">See info</a></center>
						</div>
					</div>

					<div class="card col-sm-4" style="width: 13rem;">
						<img src="images/book.png" class="card-img-top" alt="...">
						<div class="card-body">
							<h5 class="card-title text-center">BOOK</h5>
							<p class="card-text text-center">
								เวลาที่เราไม่มีอะไรเป็นของเราเลยนั่นแหละเป็นเวลาที่เรามีความสุขที่สุด</p>
							<center><a href="book.php" class="btn btn-primary mt-4">See info</a></center>
						</div>
					</div>

					<div class="card col-sm-4" style="width: 13rem;">
						<img src="images/librarian.png" class="card-img-top " alt="...">
						<div class="card-body">
							<h5 class="card-title text-center">LIBRARIAN</h5>
							<p class="card-text text-center">โกรธทำไม เสียสมองเปล่า ๆ ทะเลาะทำไม เสียเวลาเปล่า ๆ</p>
							<center><a href="librarian.php" class="btn btn-primary mt-4">See info</a></center>
						</div>
					</div>
					<div class="col-12" style="height:10px"></div>
					<div class="col-4"></div>
					<div class="col-4" style="margin-left: 45%; margin-right: auto;">
						<button class="btn btn-danger pr-3" onclick="window.location.href='logout.php'">Logout</button>
					</div>
				</div>
			</div>

		</div>
	</div>
</body>

</html>