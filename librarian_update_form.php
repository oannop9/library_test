<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>update</title>
    <link rel="stylesheet" type="text/css" href="css/modal.css">
</head>

<body>

    <?php
        $id_edit=$_GET['id_edit'];
        include"connect.php";
        $sql="SELECT * FROM librarian WHERE librarian_id=$id_edit ";
        $result = mysqli_query($conn,$sql);
        while($record = mysqli_fetch_array($result)){
            $id_edit=$record['librarian_id'];
            $name_edit=$record['librarian_name'];
            $pass_edit=$record['librarian_pass'];
            $add_edit=$record['librarian_add'];
            $tel_edit=$record['librarian_tel'];


        }
    
    
    
    ?>
    <div class="container-login100">
        <form class="modal-content container-login100" action="librarian_update.php" method="POST">
            <div class="container">
                <h1>Add Information</h1>
                <hr>
                <label for="Id"><b>Id</b></label>
                <input type="text" placeholder="id" name="id" id="id" value="<?=$id_edit?>" required>

                <label for="name"><b>Name</b></label>
                <input type="text" placeholder="Name" name="name" id="name" value="<?=$name_edit?>" required>

                <label for="Pass"><b>Pass</b></label>
                <input type="text" placeholder="pass" name="pass" id="pass" value="<?=$pass_edit?>" required>

                <label for="Address"><b>Address</b></label>
                <input type="text" placeholder="publishing" name="add" id="add" value="<?=$add_edit?>" required>

                <label for="Tel"><b>Tel</b></label>
                <input type="text" placeholder="tel" name="tel" id="tel" value="<?=$tel_edit?>" required>

                <p></p>

                <div class="clearfix">
                <a href="librarian.php"> <button type="button" class="cancelbtn">Cancel</button></a>
                    <button type="submit" class="signupbtn">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>