<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>update</title>
    <link rel="stylesheet" type="text/css" href="css/modal.css">
</head>

<body>

    <?php
        $id_edit=$_GET['id_edit'];
        include"connect.php";
        $sql="SELECT * FROM member WHERE Mem_id=$id_edit ";
        $result = mysqli_query($conn,$sql);
        while($record = mysqli_fetch_array($result)){
            $id_edit=$record['Mem_id'];
            $name_edit=$record['Mem_name'];
            $pws_edit=$record['Mem_pass'];
            $group_edit=$record['Mem_group'];
            $tel_edit=$record['Mem_tel'];
            $type_edit=$record['Mem_type'];
            $add_edit=$record['Mem_add'];

        }
    
    
    
    ?>
    <div class="container-login100">
        <form class="modal-content container-login100" action="member_update.php" method="POST">
            <div class="container">
                <h1>Add Information</h1>
                <hr>
                <label for="name"><b>Id</b></label>
                <input type="text" placeholder="id" name="id" id="id" value="<?=$id_edit?>" required>

                <label for="name"><b>Name</b></label>
                <input type="text" placeholder="Name" name="name" id="name" value="<?=$name_edit?>" required>

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Password" name="psw" id="psw" value="<?=$pws_edit?>" required>

                <label for="group"><b>Group</b></label>
                <input type="text" placeholder="Group" name="group" id="group" value="<?=$group_edit?>" required>

                <label for="tel"><b>Tel.</b></label>
                <input type="text" placeholder="telephone" name="tel" id="tel" value="<?=$tel_edit?>" required>

                <label for="type"><b>Type</b></label>
                <input type="text" placeholder="type" name="type" id="type" value="<?=$type_edit?>" required>

                <label for="add"><b>Address</b></label>
                <input type="text" placeholder="Address" name="add" id="add" value="<?=$add_edit?>" required>
                <p></p>

                <div class="clearfix">
                <a href="member.php"> <button type="button" class="cancelbtn">Cancel</button></a>
                    <button type="submit" class="signupbtn">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>