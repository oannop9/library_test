<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Member</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/modal.css">
</head>

<body>

  <div class="container-login100">
    <div class="col">
      <div class="col">
        <section class="content">
        <h1 class="text-left col-12 p-1 text-light"><a href="main.php" class="btn btn-warning pr-3 text-center">Back</a></h1>	
          <!-- Default box -->
          <form action="member_regis.php" method="POST">
            <div class="card" style="width: 100%;">
              <div class="card-header">
                <h3 class="card-title">Member list</h3>
              </div>
              <div class="card-body">
                <form action="member_form.php">
                  <table class="table table-striped projects">
                    <thead>
                      <tr>
                        <th style="width: 2%">
                          #
                        </th>
                        <th style="width: 12%" class="text-center">
                          Name
                        </th>
                        <th style="width: 12%" class="text-center">
                          Pass
                        </th>
                        <th style="width: 12%" class="text-center">
                          Group
                        </th>
                        <th style="width: 12%" class="text-center">
                          Tel.
                        </th>
                        <th style="width: 12%" class="text-center">
                          Type
                        </th>
                        <th style="width: 12%" class="text-center">
                          Address
                        </th>

                        <th style="width: 15%">
                        </th>
                      </tr>
                    </thead>
                    <thead>
                      <?php
    include"connect.php";
    $sql = "SELECT * FROM member";
    $result = mysqli_query($conn,$sql);
    $count = 0;
    
		while($record=mysqli_fetch_array($result)){
      $count++;
			echo"
			<tbody>
			<tr>
				<th style='width: 2%'>$count</th>
				<th style='width: 12%'  class='text-center'>$record[Mem_name]</th>
				<th style='width: 12%''  class='text-center'>$record[Mem_pass]</th>
				<th style='width: 12%'  class='text-center'>$record[Mem_group]</th>
				<th style='width: 12%'  class='text-center'>$record[Mem_tel]</th>
				<th style='width: 12%''  class='text-center'>$record[Mem_type]</th>
				<th style='width: 12%'  class='text-center'>$record[Mem_add]</th>
				<td class='project-actions text-right' style='width: 15%'>
					<a class='btn btn-info btn-sm' href='member_update_form.php?id_edit=$record[Mem_id]'>	Edit</a>
					<a class='btn btn-danger btn-sm' href='member_delete.php?id_edit=$record[Mem_id]'>Delete</a>
				</td>
			</tr>
			";}
		?>

                      </tbody>
                  </table>
                  <div class="row m-3">
                    <div class="col-5"></div>
                    <div class="col-2">
                      <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Add data</button>
                    </div>
                    <div class="col-5"></div>
                  </div>

                      <div id="id01" class="modal">
                        <span onclick="document.getElementById('id01').style.display='none'" class="close"
                          title="Close Modal">&times;</span>
                        <form class="modal-content" action="member_regis.php">
                          <div class="container">
                            <h1>Add Information</h1>
                            <hr>
                            <label for="text"><b>Name</b></label>
                            <input type="text" placeholder="Name" name="name" id="name" required>

                            <label for="psw"><b>Password</b></label>
                            <input type="password" placeholder="Password" name="psw" id="psw" required>

                            <label for="email"><b>Group</b></label>
                            <input type="text" placeholder="Group" name="group" id="group" required>

                            <label for="psw"><b>Tel.</b></label>
                            <input type="text" placeholder="telephone" name="tel" id="tel" required>

                            <label for="text"><b>Type</b></label>
                            <input type="text" placeholder="type" name="type" id="type" required>

                            <label for="text"><b>Address</b></label>
                            <input type="text" placeholder="Address" name="add" id="add" required>
                            <p></p>

                            <div class="clearfix">
                              <button type="button" onclick="document.getElementById('id01').style.display='none'"
                                class="cancelbtn">Cancel</button>
                              <button type="submit" class="signupbtn">Success</button>
                            </div>
                          </div>
                        </form>
                      </div>

                      <script>
                        // Get the modal
                        var modal = document.getElementById('id01');

                        // When the user clicks anywhere outside of the modal, close it
                        window.onclick = function (event) {
                          if (event.target == modal) {
                            modal.style.display = "none";
                          }
                        }
                      </script>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
          </form>
          <!-- /.card -->

        </section>
      </div>
    </div>




  </div>

  </div>



</body>

</html>