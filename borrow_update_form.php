<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>update</title>
    <link rel="stylesheet" type="text/css" href="css/modal.css">
</head>

<body>

    <?php
        $id_edit=$_GET['id_edit'];
        include"connect.php";
        $sql="SELECT * FROM book WHERE Book_id=$id_edit ";
        $result = mysqli_query($conn,$sql);
        while($record = mysqli_fetch_array($result)){
            $id_edit=$record['Book_id'];
            $name_edit=$record['Book_name'];
            $author_edit=$record['Book_author'];
            $publis_edit=$record['Book_publishing'];
            $price_edit=$record['Book_price'];


        }
    
    
    
    ?>
    <div class="container-login100">
        <form class="modal-content container-login100" action="borrow_update.php" method="POST">
            <div class="container">
                <h1>Add Information</h1>
                <hr>
                <label for="name"><b>Id</b></label>
                <input type="text" placeholder="id" name="id" id="id" value="<?=$id_edit?>" required>

                <label for="name"><b>Name</b></label>
                <input type="text" placeholder="Name" name="name" id="name" value="<?=$name_edit?>" required>

                <label for="author"><b>author</b></label>
                <input type="text" placeholder="author" name="author" id="author" value="<?=$author_edit?>" required>

                <label for="publishing"><b>publishing</b></label>
                <input type="text" placeholder="publishing" name="publishing" id="publishing" value="<?=$publis_edit?>" required>

                <label for="price"><b>price</b></label>
                <input type="text" placeholder="price" name="price" id="price" value="<?=$price_edit?>" required>

                <p></p>

                <div class="clearfix">
                <a href="book.php"> <button type="button" class="cancelbtn">Cancel</button></a>
                    <button type="submit" class="signupbtn">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>