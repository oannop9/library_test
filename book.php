<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Book</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/modal.css">

	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.js"></script>

	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>
</head>

<body>

	<div class="container-login100">
		<div class="col">
			<div class="col">
				<section class="content">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="main.php" class="btn btn-warning pr-3 btn-lg">
									<<</a> </div> <div class="col-6">
							</div>
							<div class="col">
								<a href="borrow_form.php" class="btn btn-light pr-3 btn-lg">Borrow</a>
								<a href="return_book.php" class="btn btn-dark pr-3 btn-lg">Return</a>
							</div>
						</div>
					</div>


					<!-- Default box -->
					<form action="#" method="_GET">
						<div class="card" style="width: 100%;">
							<div class="card-header">
								<h3 class="card-title">Book list</h3>

					</form>
			</div>
			<div class="card-body">
				<form action="book_form.php">
					<table class="display table" id="table_id">
						<thead>
							<tr>
								<th style="width: 7%">
									#
								</th>
								<th style="width: 15%">
									Book Name
								</th>
								<th style="width: 15%" class="text-center">
									Book author
								</th>
								<th style="width: 15%" class="text-center">
									Book publishing
								</th>
								<th style="width: 5%" class="text-center">
									Book price
								</th>
								<th style="width: 5%" class="text-center">
									Status
								</th>

								<th style="width: 15%">
								</th>
							</tr>
						</thead>
						<thead>
						<tbody>
							<?php	
											include"connect.php";
											$sql ="SELECT * FROM book";
											$result = mysqli_query($conn,$sql);
												$count = 0;
												while($record=mysqli_fetch_array($result)){
												$count++;
												if($record['status'] == 1) {
													$status = 'ว่าง';
												  }else if($record['status'] == 0){
													$status = 'ไม่ว่าง';
												  }
												echo"
													<tr>
														<th style='width: 7%'>$count</th>
														<th style='width: 15%'  >$record[Book_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Book_author]</th>
														<th style='width: 15%'  class='text-center'>$record[Book_publishing]</th>
														<th style='width: 5%'  class='text-center'>$record[Book_price]</th>
														<th style='width: 5%'  class='text-center'>$status</th>
														<td class='project-actions text-right' style='width: 15%'>
														<a class='btn btn-info btn-sm' href='book_update_form.php?id_edit=$record[Book_id]'>Edit</a>
														<a class='btn btn-danger btn-sm' href='book_delete.php?id_edit=$record[Book_id]'>Delete</a>
													</tr>
												";}
							?>
						</tbody>
						</thead>
					</table>
					<form action="book_regis.php" method="POST">
						<h1 class="text-center p-3 text-light">
							<button class="btn btn-outline-info"
								onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Add
								data</button>
						</h1>
						<div id="id01" class="modal">
							<span onclick="document.getElementById('id01').style.display='none'" class="close"
								title="Close Modal">&times;</span>
							<form class="modal-content" action="book_regis.php">
								<div class="container">
									<h1>Add Information</h1>
									<hr>
									<label for="text"><b>Book Name</b></label>
									<input type="text" placeholder="Name" name="name" id="name" required>
									<label for="text"><b>Book author</b></label>
									<input type="text" placeholder="author" name="author" id="author" required>
									<label for="text"><b>Book publishing</b></label>
									<input type="text" placeholder="publishing" name="publishing" id="publishing"
										required>
									<label for="text"><b>Book price</b></label>
									<input type="text" placeholder="price" name="price" id="price" required>
									<p></p>
									<div class="clearfix">
										<button type="button"
											onclick="document.getElementById('id01').style.display='none'"
											class="cancelbtn">Cancel</button>
										<button type="submit" class="signupbtn">Success</button>
									</div>
								</div>
							</form>
						</div>
					</form>
			</div>
			<!-- /.card-body -->
		</div>
		</form>
		<!-- /.card -->
		</section>
	</div>
	</div>
	</div>
</body>
<script>
	$(document).ready(function () {
		$('#table_id').DataTable({
			"language": {
				"search": "ค้นหาหนังสือที่ต้องการ"
			}
		});
	});

	var modal = document.getElementById('id01');

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function (event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
</script>

</html>