<?php
	$id_search_book = $_GET['id_book'];
	include"connect.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Book</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/modal.css">

	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.js"></script>

	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>
</head>

<body>

	<div class="container-login100">
		<div class="col">
			<div class="col">
				<section class="content">
				<div class="container">

					<!-- Default box -->
					<form action="" method="get">
						<div class="card" style="width: 100%;">
							<div class="card-header">
								<h3 class="card-title">Book list</h3>
							</div>
							<!-- Search form -->
							<h1 class="text-left p-3 text-light">
								<input class="form-control" type="text" name="id_book" id="id_book" placeholder="Search Name Book" aria-label="Search"></h1>
							<div class="card-body">
								<form  style="background:wheat;">
									<table class="display table" id="table_id">
										<thead>
											<tr>
												<th style="width: 7%">
													#
												</th>
												<th style="width: 15%">
													Book Name
												</th>
												<th style="width: 15%" class="text-center">
													Book author
												</th>
												<th style="width: 15%" class="text-center">
													Book publishing
												</th>
												<th style="width: 10%" class="text-center">
													Book price
												</th>
												<th style="width: 5%" class="text-center">
													Status
												</th>

												<th style="width: 15%">
												</th>
											</tr>
										</thead>
										<thead>
											<tbody>
											<?php
											
											
											$sql = "SELECT * FROM book where Book_name LIKE '%".$id_search_book."%'";
											$result = mysqli_query($conn,$sql);
												$count = 0;
												while($record=mysqli_fetch_array($result)){
												$count++;
												if($record['status'] == 0) {
													$status = 'ว่าง';
												  }else{
													$status = 'ไม่ว่าง';
												  }
												echo"
													<tr>
														<th style='width: 7%'>$count</th>
														<th style='width: 15%'  >$record[Book_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Book_author]</th>
														<th style='width: 15%'  class='text-center'>$record[Book_publishing]</th>
														<th style='width: 10%'  class='text-center'>$record[Book_price]</th>
														<th style='width: 5%'  class='text-center'>$status</th>
														<td class='project-actions text-right' style='width: 15%'>
														<a class='btn btn-info btn-sm' href='book_update_form.php?id_edit=$record[Book_id]'>Edit</a>
														<a class='btn btn-danger btn-sm' href='book_delete.php?id_edit=$record[Book_id]'>Delete</a>
												</tr>";}
											?>

											</tbody>
										</thead>
									</table>
								</form>
							</div>
							<!-- /.card-body -->
						</div>
					</form>
					<!-- /.card -->
				</section>
			</div>
		</div>
	</div>



</body>


</html>