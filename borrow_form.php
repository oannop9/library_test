<?php 

	include("connect.php");
	$db_book = "select * from book ";
	$rs = mysqli_query($conn,$db_book);
	$db_mem = "select * from member ";
	$rf = mysqli_query($conn,$db_mem);
	$db_li = "select * from librarian ";
	$li = mysqli_query($conn,$db_li);
	$join ="SELECT * FROM borrow_return 
	inner join book on borrow_return.Book_id = book.Book_id 
	inner join member on borrow_return.Mem_id = member.Mem_id
	WHERE borrow_return.status = 0";
	$inner = mysqli_query($conn,$join)or die( mysqli_error($conn));
	// echo $join;
	

	// print_r($rs);
	


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Borrow</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/modal.css">

</head>
<body>
<div class="container-login100">
		<div class="col">
			<div class="col">
				<section class="content">
				<h1 class="text-left p-3 text-light"><a href="book.php" class="btn btn-warning pr-3 btn-lg"><<</a></h1>
					
					<!-- Default box -->
					<!-- <form action="borrow_regis.php" method="POST"> -->
						<div class="card" style="width: 100%;">
							<div class="card-header">
								<h3 class="card-title">Borrow list</h3>
							</div>
							<div class="card-body">
								
									<table class="display table" id="table_id">
										<thead>
											<tr>
												<th style="width: 7%">
													#
												</th>
												<th style="width: 15%">
													Book Name
												</th>
												<th style="width: 15%" class="text-center">
													Member Name
												</th>
												<th style="width: 10%" class="text-center">
                                                    Date Lender
												</th>
												<th style="width: 10%" class="text-center">
                                                    Date Fix Receiver
												</th>


												<th style="width: 15%">
												</th>
											</tr>
										</thead>
										<thead>
											<tbody>
											<?php
												$count = 0;
												while($record=mysqli_fetch_array($inner)){
												$count++;
												echo"
													<tr>
														<th style='width: 7%'>$count</th>
														<th style='width: 15%'  >$record[Book_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Mem_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Date_borrow]</th>
														<th style='width: 10%'  class='text-center'>$record[Count_date_borrow]</th>
														<td class='project-actions text-right' style='width: 15%'>
															<a class='btn btn-warning btn-sm' href='return.php?data1=$record[Book_id]&data2=$record[id]'>RETURN</a>
														    <a class='btn btn-danger btn-sm' href='borrow_delete.php?id_del=$record[id]'>Delete</a>
												    </tr>";}
											?>
											</tbody>
										</thead>
									</table>
									<h1 class="text-center p-3 text-light">
											<button class="btn btn-outline-info" onclick="document.getElementById('id01').style.display='block'"
												style="width:auto;">Borrow</button>
									</h1>
									<div id="id01" class="modal">
										<span onclick="document.getElementById('id01').style.display='none'"
											class="close" title="Close Modal">&times;</span>
										<form class="modal-content" action="borrow_regis.php" method="POST" >
											<div class="container">
												<h1>Add Borrow</h1>
												<hr>
												<label for="text"><b>Book Name</b></label>
                                                    <select class="form-control" id="book" name="book">
													<?php  while ($row = mysqli_fetch_array($rs)) { ?>
														<option value="<?php echo $row['Book_id']?>"><?php echo $row['Book_name'] ?></option>
													<?php  }?>
                                                       
                                                       
                                                    </select>

												<label for="text"><b>Member Name</b></label>
												<select class="form-control" id="mam" name="mam">
													<?php while ($col = mysqli_fetch_array($rf)) {?>
														<option value="<?php echo $col['Mem_id']?>"><?php echo $col['Mem_name'] ?></option>
													<?php }?>
													</select>
													
													<label for="text"><b>Staff Borrow Name</b></label>
                                                    <select class="form-control" id="staff_bo" name="staff_bo">
													<?php  while ($bow = mysqli_fetch_array($li)) { ?>
														 <option value="<?php echo $bow['librarian_id']?>"><?php echo $bow['librarian_name'] ?></option>
													<?php  }?>
                                                       
                                                       
													</select>
													

												<label for="text"><b>Date Lender</b></label>
												<input type="date" id="date_borrow" name="date_borrow">
												<label for="text"><b>Date fix Return</b></label>
												<input type="date" id="fix_date" name="fix_date">
												<p></p>

												<div class="clearfix">
													<button type="button" 
														onclick="document.getElementById('id01').style.display='none'"
														class="cancelbtn">Cancel</button>
													<button type="submit" class="signupbtn">Success</button>
												</div>
											</div>
										</form>
									</div>
								
							</div>
							<!-- /.card-body -->
						</div>
					</form>
					<!-- /.card -->
				</section>
			</div>
		</div>
	</div>



</body>
<script>
// Get the modal
var modal = document.getElementById('id01');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

</html>
    
</body>
</html>