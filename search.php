<?
	session_start();
	if($_SESSION['sess.userid']=="" || $_SESSION['sess.username']=="")
	{
		header('location:login.php');
		exit;
		}


?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SEARCH BOOK</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.js"></script>

	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>




</head>

<body>
	<div class="container-login100">
		<div class="col">
			<div class="col">
				<section class="content">
					<!-- Default box -->
					<form action="book_regis.php" method="POST">
						<div class="card" style="width: 100%;">
							<div class="card-header">
								<h3 class="card-title">Book list</h3>
							</div>

							<div class="card-body">
								<form action="book_form.php">
									<table class="display" id="table_id">
										<thead>
											<tr>
												<th style="width: 2%">
													#
												</th>
												<th style="width: 15%" >
													Book Name
												</th>
												<th style="width: 25%" class="text-center">
													Book author
												</th>
												<th style="width: 25%" class="text-center">
													Book publishing
												</th>
												<th style="width: 10%" class="text-center">
													Book price
												</th>
												<th style="width: 10%" class="text-center">
													Status
												</th>
											</tr>
										</thead>
										<thead>
											<tbody>
												<?php
													include"connect.php";
													$sql = "SELECT * FROM book";
													$result = mysqli_query($conn,$sql);
													$count = 0;
													while($record=mysqli_fetch_array($result)){
													$count++;
													if($record['status'] == 0) {
														$status = 'ว่าง';
													  }else{
														$status = 'ไม่ว่าง';
													  }
													echo"<tr>
															<th style='width: 4%'   class='text-center'>$count</th>
															<th style='width: 15%'  >$record[Book_name]</th>
															<th style='width: 25%'  class='text-center'>$record[Book_author]</th>
															<th style='width: 25%'  class='text-center'>$record[Book_publishing]</th>
															<th style='width: 10%'  class='text-center'>$record[Book_price]</th>
															<th style='width: 10%'  class='text-center'>$status</th>
														</tr>";}
												?>
											</tbody>
										</thead>
									</table>
								</form>
							</div>
						</div>
					</form>
				</section>

				<h1 class="text-center p-3 text-light"><a href="logout.php" class="btn btn-danger pr-3 align-text-center">Logout</a></h1>

				</div>
			</div>
		</div>
	</div>
</body>

<script>
	$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>

</html>