-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2020 at 06:50 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `Book_id` int(5) NOT NULL,
  `Book_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Book_author` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Book_publishing` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Book_price` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = ไม่ว่าง. 1 = ว่าง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`Book_id`, `Book_name`, `Book_author`, `Book_publishing`, `Book_price`, `status`) VALUES
(12, 'Plants vs Zombies', 'Xiao jiangnan', 'ประภาพร รุ่งขจรวงศ์', '75', 1),
(13, 'เอาชีวิตรอดในโลกของสัตว์', 'ภาสกร รัตนสุวรรณ', 'นานมีบุ๊คส์, บจก.', '150', 0),
(14, 'แบบฝึกความเข้าใจ', 'วาสนา ทองการุณ', 'เดอะบุคส์, บจก.', '109', 1),
(15, 'หนังสือเสริมการเรียน', 'เฉลิมชัย มอญสุขำ', 'เดอะบุคส์, บจก.', '104', 1);

-- --------------------------------------------------------

--
-- Table structure for table `borrow_return`
--

CREATE TABLE `borrow_return` (
  `id` int(5) NOT NULL,
  `Book_id` int(5) NOT NULL,
  `Mem_id` int(5) NOT NULL,
  `Staff_id_borrow` int(5) NOT NULL,
  `Staff_id_return` int(5) NOT NULL,
  `Date_borrow` datetime NOT NULL,
  `Count_date_borrow` datetime NOT NULL,
  `Date_return` datetime NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = ไม่ว่าง. 1 = ว่าง	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borrow_return`
--

INSERT INTO `borrow_return` (`id`, `Book_id`, `Mem_id`, `Staff_id_borrow`, `Staff_id_return`, `Date_borrow`, `Count_date_borrow`, `Date_return`, `status`) VALUES
(1, 11, 1, 1, 1, '2020-02-28 08:50:30', '2020-02-28 08:50:30', '2020-02-28 08:50:30', 1),
(14, 0, 1, 0, 0, '2020-02-28 00:00:00', '2020-02-28 00:00:00', '0000-00-00 00:00:00', 1),
(17, 12, 5, 2, 0, '2020-02-07 00:00:00', '2020-02-08 00:00:00', '0000-00-00 00:00:00', 1),
(18, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(19, 11, 1, 1, 0, '2020-02-28 00:00:00', '2020-02-29 00:00:00', '0000-00-00 00:00:00', 1),
(20, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(21, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(22, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(23, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(24, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(25, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(26, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(27, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(28, 11, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(30, 13, 5, 2, 0, '2020-02-28 00:00:00', '2020-02-28 00:00:00', '0000-00-00 00:00:00', 1),
(31, 13, 1, 1, 0, '2020-03-01 00:00:00', '2020-03-27 00:00:00', '0000-00-00 00:00:00', 1),
(32, 13, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(33, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE `librarian` (
  `librarian_id` int(5) NOT NULL,
  `librarian_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `librarian_pass` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `librarian_add` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `librarian_tel` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `librarian`
--

INSERT INTO `librarian` (`librarian_id`, `librarian_name`, `librarian_pass`, `librarian_add`, `librarian_tel`) VALUES
(1, 'boonsongs', '1234', 'samutprakan', 123456789),
(2, 'somsai', '1234', 'bangkok', 987654312);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(5) NOT NULL,
  `username` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mem_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `Mem_name`, `level`) VALUES
(1, 'admin', '1234', 'admin', 'admin'),
(2, 'user', '1234', 'user', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `Mem_id` int(5) NOT NULL,
  `Mem_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mem_pass` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mem_group` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mem_tel` int(10) NOT NULL,
  `Mem_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mem_add` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`Mem_id`, `Mem_name`, `Mem_pass`, `Mem_group`, `Mem_tel`, `Mem_type`, `Mem_add`) VALUES
(1, 'apistia', '1234', 'pws', 123456789, 'student', 'samutprakarn'),
(3, 'oannop', '1234', 'student', 154879515, 'student', 'Bangkok'),
(5, 'Pang', '213454', 'student', 31548486, 'student', 'sukhothai'),
(6, 'กมลภพ', '11154', 'IT', 1594896, 'student', 'Bang Pu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`Book_id`);

--
-- Indexes for table `borrow_return`
--
ALTER TABLE `borrow_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
  ADD PRIMARY KEY (`librarian_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`Mem_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `Book_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `borrow_return`
--
ALTER TABLE `borrow_return`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `librarian`
--
ALTER TABLE `librarian`
  MODIFY `librarian_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `Mem_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
