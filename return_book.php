<?php 

	include("connect.php");
	$db_book = "select * from book ";
	$rs = mysqli_query($conn,$db_book);
	$db_mem = "select * from member ";
	$rf = mysqli_query($conn,$db_mem);
	$db_li = "select * from librarian ";
	$li = mysqli_query($conn,$db_li);


	$join ="select * from borrow_return 
	inner join book on borrow_return.Book_id = book.Book_id 
	inner join member on borrow_return.Mem_id = member.Mem_id";
	$inner = mysqli_query($conn,$join);
	// echo $join;
	

	// print_r($rs);
	


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Borrow</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/modal.css">

</head>
<body>
<div class="container-login100">
		<div class="col">
			<div class="col">
				<section class="content">
				<h1 class="text-left p-3 text-light"><a href="book.php" class="btn btn-warning pr-3 btn-lg"><<</a></h1>
					
					<!-- Default box -->
					<!-- <form action="borrow_regis.php" method="POST"> -->
						<div class="card" style="width: 100%;">
							<div class="card-header">
								<h3 class="card-title">Return list</h3>
							</div>
							<div class="card-body" >
								
									<table class="display table" id="table_id">
										<thead>
											<tr>
												<th style="width: 7%">
													#
												</th>
												<th style="width: 20%">
													Book Name
												</th>
												<th style="width: 15%" class="text-center">
													Member Name
												</th>
												<th style="width: 10%" class="text-center">
                                                    Date Lender
												</th>
												<th style="width: 20%" class="text-center">
                                                    Date Fix Receiver
												</th>
											</tr>
										</thead>
										<thead>
											<tbody>
											<?php
												$count = 0;
												while($record=mysqli_fetch_array($inner)){
												$count++;
												echo"
													<tr>
														<th style='width: 7%'>$count</th>
														<th style='width: 20%'  >$record[Book_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Mem_name]</th>
														<th style='width: 15%'  class='text-center'>$record[Date_borrow]</th>
														<th style='width: 20%'  class='text-center'>$record[Count_date_borrow]</th>
												    </tr>";}
											?>
											</tbody>
										</thead>
									</table>								
							</div>
							<!-- /.card-body -->
						</div>
					</form>
					<!-- /.card -->
				</section>
			</div>
		</div>
	</div>



</body>
<script>
	$(document).ready(function () {
		$('#table_id').DataTable({
			"language": {
				"search": "ค้นหาหนังสือที่ต้องการ"
			}
		});
	});


</script>

</html>
    
</body>
</html>