<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>librarain list</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/modal.css">
	<link rel="stylesheet" type="text/css" href="DataTables/datatables.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.js"></script>

	<script type="text/javascript" charset="utf8" src="DataTables/datatables.js"></script>
</head>

<body>

	<div class="container-login100">
		<div class="container-fluid">
			<div class="col">
				<div class="col">
					<section class="content">
					<h1 class="text-left col-12 p-1 text-light"><a href="main.php" class="btn btn-warning pr-3 text-center">back</a></h1>	
						<!-- Default box -->
						<form action="librarian_regis.php" method="POST">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">LIBRARIAN</h3>
								</div>
								<div class="card-body">
									<form action="librarian_form.php">
										<table class="display" id="table_id">
											<thead>
												<tr>
													<th style="width: 2%">
														#
													</th>
													<th style="width: 15%" class="text-center">
														Name
													</th>
													<th style="width: 15%" class="text-center">
														Pass
													</th>
													<th style="width: 25%" class="text-center">
														Address
													</th>
													<th style="width: 10%" class="text-center">
														Tel.
													</th>

													<th style="width: 20%">
													</th>
												</tr>
											</thead>
											<thead>
											<tbody>
												<?php
													include"connect.php";
													$sql = "SELECT * FROM librarian";
													$result = mysqli_query($conn,$sql);
													$count = 0;
													while($record=mysqli_fetch_array($result)){
													$count++;
													echo"
														<tr>
															<th style='width: 2%'>$count</th>
															<th   class='text-center'>$record[librarian_name]</th>
															<th  class='text-center'>$record[librarian_pass]</th>
															<th  class='text-center'>$record[librarian_add]</th>
															<th   class='text-center'>$record[librarian_tel]</th>
															<td class='project-actions text-right' style='width: 25%'>
																<a class='btn btn-info btn-sm' href='librarian_update_form.php?id_edit=$record[librarian_id]'>Edit</a>
																<a class='btn btn-danger btn-sm' href='librarian_delete.php?id_edit=$record[librarian_id]'>Delete</a>
															</td>
														</tr>";}
											?>
												</tbody>
										</table>
										<div class="row m-3">
											<div class="col-5"></div>
											<div class="col-2">
												<button onclick="document.getElementById('id01').style.display='block'"
													style="width:auto;">Add data</button>
											</div>
											<div class="col-5"></div>
										</div>
										<div id="id01" class="modal">
											<span onclick="document.getElementById('id01').style.display='none'"
												class="close" title="Close Modal">&times;</span>
											<form class="modal-content" action="librarian_regis.php">
												<div class="container">
													<h1>Add Information</h1>
													<hr>
													<label for="text"><b>Librarian Name</b></label>
													<input type="text" placeholder="Name" name="name" id="name"
														required>

													<label for="text"><b>Librarian Pass</b></label>
													<input type="password" placeholder="pass" name="pass" id="pass"
														required>

													<label for="text"><b>Librarian Address</b></label>
													<input type="text" placeholder="add" name="add" id="add" required>

													<label for="text"><b>Librarian Tel</b></label>
													<input type="text" placeholder="Tel" name="tel" id="tel" required>


													<p></p>

													<div class="clearfix">
														<button type="button"
															onclick="document.getElementById('id01').style.display='none'"
															class="cancelbtn">Cancel</button>
														<button type="submit" class="signupbtn">Success</button>
													</div>
												</div>
											</form>
										</div>

										<script>
											// Get the modal
											var modal = document.getElementById('id01');

											// When the user clicks anywhere outside of the modal, close it
											window.onclick = function (event) {
												if (event.target == modal) {
													modal.style.display = "none";
												}
											}
										</script>
									</form>
								</div>
								<!-- /.card-body -->
							</div>
						</form>
						<!-- /.card -->

					</section>
				</div>
			</div>
		</div>
	</div>



</body>
<script>
	$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>

</html>